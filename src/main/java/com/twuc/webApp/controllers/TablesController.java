package com.twuc.webApp.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/tables")
public class TablesController {
    @GetMapping("/plus")
    public String getTableOfPlus(
            @RequestParam(defaultValue = "1") Integer start, @RequestParam(defaultValue = "9") Integer end) {
        if (start >= end) throw new IllegalArgumentException();
        return calculate("+", start, end);
    }

    @GetMapping("/multiply")
    public String getTableOfMultiply(
            @RequestParam(defaultValue = "1") Integer start, @RequestParam(defaultValue = "9") Integer end) {
        if (start >= end) throw new IllegalArgumentException();
        return calculate("*", start, end);
    }

    private String calculate(String operation, Integer start, Integer end) {
        StringBuilder result = new StringBuilder();
        result.append("<pre>");
        for (int i = start; i <= end; i++) {
            for (int j = 1; j <= i; j++) {
                int res = operation.equals("+") ? i + j : i * j;
                String format = String.format("%d" + operation + "%d=%d", i, j, res);
                result.append(format);
                if (i != j) {
                    result.append(res >= 10 ? " " : "  ");
                }
            }
            result.append("\n");
        }
        result.append("</pre>");
        return String.valueOf(result);
    }

    @ExceptionHandler
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
    }
}
