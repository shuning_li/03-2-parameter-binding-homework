package com.twuc.webApp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class TablesControllerExceptionHandleTest {
    @Autowired
    TestRestTemplate template;

    @Test
    void plus_table_should_get_400_when_params_not_correct() {
        ResponseEntity<String> entity = template.getForEntity("/api/tables/plus?start=4&end=1", String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void multiply_table_should_get_400_when_params_not_correct() {
        ResponseEntity<String> entity = template.getForEntity("/api/tables/multiply?start=4&end=1", String.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }
}
